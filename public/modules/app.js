import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getFirestore,
  deleteDoc,
  updateDoc,
  getDoc,
  getDocs,
  setDoc,
  doc,
  where,
  query,
  collection,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,
  signInWithPopup,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

// import { addMovieToDB } from "./createNewItemToDB.js";
// import { listenerForUpdateQty } from "./updateDB.js";
// import { renderAllMoviesFromDB } from "./renderHTML";

const firebaseConfig = {
  apiKey: "AIzaSyBBqTe2o60-tHoURtKzwcTgLevKrynKRhE",
  authDomain: "fir-app-train-879cc.firebaseapp.com",
  databaseURL:
    "https://fir-app-train-879cc-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fir-app-train-879cc",
  storageBucket: "fir-app-train-879cc.appspot.com",
  messagingSenderId: "640047476886",
  appId: "1:640047476886:web:0e50f80f08c39bf92812b7",
  measurementId: "G-KG3PTRET3Q",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const provider = new GoogleAuthProvider();

const formQS = {
  formEmail: document.querySelector("#formEmail"),
  formPassword: document.querySelector("#formPassword"),
  btnLogin: document.querySelector("#btnLogin"),
  lblErrorMessage: document.querySelector("#lblErrorMessage"),
  btnSignup: document.querySelector("#btnSignup"),
  btnLogout: document.querySelector("#btnLogout"),
  userAuth: document.querySelector("#userAuth"),
  loginForm: document.querySelector("#form"),
  signWithGoogle: document.querySelector("#google"),
};

const loginUser = async () => {
  const valueEmail = formQS.formEmail.value;
  const valuePassword = formQS.formPassword.value;
  try {
    const user = await signInWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log(user);
  } catch (error) {
    console.log(error);
    if (error.code === AuthErrorCodes.INVALID_PASSWORD)
      formQS.lblErrorMessage.innerText = "Invalid password";
  }
};

// // addEvent login
formQS.signWithGoogle.addEventListener("click", () => {
  signInWithPopup(auth, provider)
    .then((result) => {
      const credential = GoogleAuthProvider.credentialFromResult(result);
      const token = credential.accessToken;

      const user = result.user;
      console.log("User logged in");
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      const email = error.email;
      const credential = GoogleAuthProvider.credentialFromError(error);
      console.error(errorCode.errorMessage);
    });
});
btnLogin.addEventListener("click", loginUser);

//dodanie uzytkowinika
const createUser = async () => {
  const valueEmail = formEmail.value;
  const valuePassword = formPassword.value;

  try {
    const user = await createUserWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log("User created sucessfully");
  } catch (error) {
    if (error) {
      lblErrorMessage.innerHTML = "User not created";
    }

    console.error(error);
  }
};
formQS.btnSignup.addEventListener("click", createUser);
// //logout

const logoutUser = async () => {
  await signOut(auth);
  console.log("Log out sucessfully");
};

formQS.btnLogout.addEventListener("click", logoutUser);

// 1. Zalozyc konto Google/konto Firebase.
// 2. Stworzyć projekt w Firebase.
// 3. Sprawdzić wszystkie zakładki i obejrzeć docs/filmy do tych które Cię interesują.
// 4. Instalcja środowiska
// 5. Stworzenie projektu webowego w Firebase
// 6. Stworznie ręcznie kilku dokumentów w bazie
// 7. Wykoanenie wszystkich operacji CRUD (z wyświetlaniem efektów w konsoli)
// 8*. Stworzenie UI do wyświetlania pobranych dokumentów z bazy danych
// 9**. Dodanie możliwości usuwanie rekordu z poziomu UI
// 10***. Dodanie możliwości aktualizacji rekordu z poziomu UI

const db = getFirestore(app);

const moviesCollection = "Movies";

const docRef = doc(db, moviesCollection, "1");
console.log(moviesCollection, db);
const docSnap = await getDoc(docRef);

if (docSnap) {
  console.log(docSnap);
}
//create item
// const newMovieRef = doc(db, moviesCollection, "4");

// const movie = {
//   1: "Lord of the Rings",
//   2: {
//     forRent: true,
//     copiesAvaible: 999,
//   },
// };

// setDoc(newMovieRef, movie);
// console.log(movie);
//update document

// const movieForUpdateRef = doc(db, moviesCollection, "3");

// const updateDataForMovie = {
//   2: {
//     forRent: false,
//     copiesAvaible: 0,
//   },
// };
// await updateDoc(movieForUpdateRef, updateDataForMovie);
// console.log(updateDataForMovie);
//delete item\

// await deleteDoc(doc(db, moviesCollection, "3"));

const contentDiv = document.querySelector(".content");
const addMovieQuerySelectors = {
  qty: document.querySelector("#qty"),
  addMovieInput: document.querySelector("#movie"),
  addMovieBtn: document.querySelector(".confirm"),
};
const allAvaibleMovies = collection(db, moviesCollection);
const querySnapshot = await getDocs(allAvaibleMovies);

const renderAllMoviesFromDB = async () => {
  await querySnapshot.forEach((doc) => {
    contentDiv.innerHTML += `
        <div class="movie">
      <h3>${doc.data()[1]}</h3>
      <p>Quantity: ${doc.data()[2].copiesAvaible}</p>
     <button type="button" data-id="${doc.id}" class='btnDel'>Delete</button>
      <button type="button" data-id="${doc.id}"class='btnUpd'>Update</button>
      </div>`;
    //     contentDiv.innerHTML += `
    //    <table id="table">
    // <tr class="movie">
    // <td > <h3>${doc.data()[1]}</h3></td>
    // <td><p>Quantity: ${doc.data()[2].copiesAvaible}</p></td>
    // <td><button data-id="${doc.id}" class='btnDel'>Delete</button></td>
    // <td> <button data-id="${doc.id}"class='btnUpd'>Update</button></td>
    // </tr>
    // </table>
  });
};

const reloadWindowTimeout = () => {
  setTimeout(window.location.reload(), 2000);
};
const addMovieToDB = async () => {
  const btn = document.querySelectorAll(".btnUpd");
  const lastId = btn[btn.length - 1].dataset.id;
  const newMovieRef = doc(db, moviesCollection, `${parseInt(lastId) + 1}`);
  const newMovie = {
    1: `${addMovieQuerySelectors.addMovieInput.value}`,
    2: {
      forRent: addMovieQuerySelectors.qty.value ? true : false,
      copiesAvaible: addMovieQuerySelectors.qty.value,
    },
  };
  if (
    addMovieQuerySelectors.addMovieInput.value &&
    addMovieQuerySelectors.qty.value
  ) {
    contentDiv.innerHTML = "";
    await setDoc(newMovieRef, newMovie);
    reloadWindowTimeout();
  }
};

addMovieQuerySelectors.addMovieBtn.addEventListener("click", addMovieToDB);

const updateButtonFunction = async (e) => {
  const input = document.querySelector(".upd");
  const movieForUpdateRef = doc(db, moviesCollection, e.target.dataset.id);
  await updateDoc(movieForUpdateRef, {
    2: {
      copiesAvaible: input.value,
      forRent: true,
    },
  });
  contentDiv.innerHTML = "";
  renderAllMoviesFromDB();
  reloadWindowTimeout();
};

const isCorrectField = (e, HTMLelement) => {
  if (e.target.nodeName == HTMLelement) {
    return (e.target.innerHTML = `Edit quantity: <input class="upd" min="1"type="number" value="${e.target.innerHTML
      .match(/\d/g)
      .join("")}"/>`);
  }
};
const listenerForUpdateQty = async (e) => {
  const updateButtons = document.querySelectorAll(".btnUpd");
  console.log(e);
  const quantityEditField = "P";
  if (isCorrectField(e, quantityEditField))
    updateButtons.forEach((button) => {
      button.addEventListener("click", updateButtonFunction);
    });
};

const deleteRecordFromDB = async (e) => {
  await deleteDoc(doc(db, moviesCollection, e.target.dataset.id));
  contentDiv.innerHTML = "";
  renderAllMoviesFromDB();
  reloadWindowTimeout();
};

const authObserve = async () => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      renderAllMoviesFromDB();
      const qsDelBtn = document.querySelectorAll(".btnDel");
      const qsUpdate = document.querySelectorAll(".movie");
      qsDelBtn.forEach((movie) => {
        movie.addEventListener("click", deleteRecordFromDB);
      });

      qsUpdate.forEach((movie) => {
        movie.addEventListener("click", listenerForUpdateQty);
      });
      formQS.loginForm.style.display = "none";
      formQS.userAuth.style.display = "block";
    } else {
      formQS.loginForm.style.display = "block";
      formQS.userAuth.style.display = "none";
      contentDiv.innerHTML = "";
    }
  });
};

authObserve();
